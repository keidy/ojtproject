FROM nginx/unit:1.26.1-python3.9
WORKDIR /usr/local/nginx/app
COPY config/conf.json /docker-entrypoint.d/
COPY . /usr/local/nginx/app/

RUN \
    apt update && apt -y upgrade && apt install -y python3.9 python3-pip && \
    pip3 install flask boto3 pymysql flask_sqlalchemy python-dotenv requests
