var region = 'ap-northeast-2';
$(document).ready(function () {
  $(".snapshots>h3").text("All");
  $(".snapshots>ul").empty();
  console.log(region);
  $.ajax({
    type: "GET",
    url: "/list/" + region,
    dataType: "json",
    success: function (data) {
      init_table(data);
    },
  });
});
$(function () {
  $("#regions").change(function () {
    $(".snapshots>ul").empty();
    $(".snapshots>h3").text("");
    region = this.value;
    console.log(region);
    $.ajax({
      type: "GET",
      url: "/list/" + region,
      dataType: "json",
      success: function (data) {
        init_table(data);
      },
    });
  });
});

function init_table(data) {
  let el = $("tbody");
  el.empty();
  $.each(data, function (index, value) {
    let str =
      "<tr id=" +
      index +
      ">" +
      "<td>" +
      value.id +
      "</td>" +
      '<td id="name">' +
      value.name +
      "</td>" +
      '<td style="text-align: center;"><button class="check" style="width: 100%;">check</button></td>' +
      '<td id="mark"><span>&hellip;</span></td>' +
      "</tr>";
    el.append(str);
    check_snapshot(index, value.name);
  });
  $(".check").each(function (i) {
    $(this).on("click", function (e) {
      $(".snapshots>ul").empty();
      let click_name = $(this).parents().children()[2].innerText;
      $(".snapshots>h3").text(click_name);
      // console.log($(".snapshots>h3").text());
      check_snapshot(i, click_name);
    });
  });
}
// &#10004; check
// &hellip; ...
// &#10060; x
function check_snapshot(index, name) {
  let snapshots = []
  $("#" + index + " span").html("&hellip;");
  $.ajax({
    type: "POST",
    accept: "application/json",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({'name': name, 'region':region}),
    url: "/snapshots",
    dataType: "json",
    success: function (data) {
      console.log(data);
      if (data.result == "success") {
        $("#" + index + " span").html("&#10004;");
      } else {
        $("#" + index + " span").html("&#10060;");
      }
      let el = $(".snapshots>ul");
      $.each(data.snapshots, function(index, value){
        if (!snapshots.includes(value)) {
          let str = '<li>'+ value +'</li>';
          snapshots.push(value);
          el.append(str);
        }
      });
    },
  });
}