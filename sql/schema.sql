create table log(
	`no` int(10) auto_increment,
  `ip` char(16) not null,
  `name` varchar(30),
  `req_time` timestamp,
  `method` char(8),
  `result` char(8),
	`url` varchar(30),
  primary key (`no`)
) engine=InnoDB charset=utf8;