from glob import glob
from tabnanny import verbose
from flask import Flask, request, url_for, render_template, flash, jsonify
import boto3
from datetime import date, datetime, timedelta
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from model import model
from config import config

AWS_REGION = "ap-northeast-2"

app = Flask(__name__)

app.config.from_object(config)

db = SQLAlchemy()
db.init_app(app)

inst_name = ""


def desc_ec2(region):
    ec2_list = []
    # region을 지정해서 해당 리전 내의 인스턴스 목록 가져옵니다.
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.describe_instances(Filters=[
        {
            "Name": "instance-state-name",
            "Values": ["running", "stopped"],
        }
    ])
    for instances in response['Reservations']:
        id = ""
        name = ""
        # 모든 인스턴스에서 이름과 id를 가져 옵니다.
        for instance in instances['Instances']:
            # print(instance)
            id = instance['InstanceId']
            name = '-'
            if 'Tags' in instance: 
                for tag in instance['Tags']:
                    if 'Name' in tag['Key'] and tag['Key'] == 'Name':
                        name = tag['Value']
        data = dict(id=id, name=name)
        ec2_list.append(data)
        ec2_list = sorted(ec2_list, key=lambda d: d['name'], reverse=True)
    return ec2_list


def get_dates():
    dates = []
    today = date.today().isoformat().replace('-', '')
    dates.append(today)
    day1 = datetime.now() - timedelta(1)
    day2 = datetime.now() - timedelta(2)
    dates.append(datetime.strftime(day1, '%Y%m%d'))
    dates.append(datetime.strftime(day2, '%Y%m%d'))
    return dates


@app.route('/snapshots', methods=["POST"])
def post_snapshots():
    data = request.json
    name = data['name']
    region = data['region']
    # 인스턴스의 이름을 가진 스냅샷을 찾습니다.
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.describe_snapshots(Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                name+'*',
            ]
        },
    ], OwnerIds=['self'])
    # 오늘 날짜부터 3일 전까지를 리스트로 만들어 둡니다.
    dates = get_dates()
    # 3일 전까지 스냅샷이 없거나 스냅샷의 개수가 3개 미만이면 fail을 리턴합니다.
    snapshots = []
    if len(response["Snapshots"]) < 3:
        save_log(name, "fail")
        return jsonify(dict(name=name, result="fail"))
    else:
        if len(response["Snapshots"]) >= 6:
            for i in range(1, int(len(response["Snapshots"]) / 3)):
                dates = dates + get_dates()
        for snapshot in response["Snapshots"]:
            snapshots.append(snapshot["Description"])
            parse_name = snapshot["Tags"][0]["Value"].split('-')
            if parse_name[-1] not in dates:
                save_log(name, "fail")
                return jsonify(dict(name=name, result="fail"))
            dates.remove(parse_name[-1])
    save_log(name, "success")
    return jsonify(dict(name=name, result="success", snapshots=snapshots))


def save_log(name, result):
    data = model.Log(
        0,
        request.environ.get('HTTP_X_REAL_IP', request.remote_addr),
        name,
        datetime.now(),
        request.method,
        result,
        request.path
    )
    model.db.session.add(data)
    model.db.session.commit()
    model.db.session.remove()

import urllib.request
@app.route('/')
def init():
    # print(db)
    # req = requests.get("http://ipconfig.kr")
    public_ip = urllib.request.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4").read()
    print("IP Address(External) : ", public_ip)
    if 'SERVER_ADDR' in request.environ:
        private_ip = request.environ['SERVER_ADDR']
    else:
        private_ip = request.host.split(":")[0]
    return render_template('index.html', hostname=request.host.split(":")[0], environ=private_ip, public_ip=public_ip)


@app.route('/list', methods=["GET"])
def get_ec2():
    ec2_list = desc_ec2(AWS_REGION)
    return jsonify(ec2_list)


@app.route('/list/<region>', methods=["GET"])
def get_ec2_by_region(region):
    ec2_list = desc_ec2(region)
    return jsonify(ec2_list)


@app.route('/log')
def get_logs():
    rows = model.Log.query.order_by(desc(model.Log.req_time)).all()
    datas = []
    for e in rows:
        data = dict(ip=e.ip, name=e.name, req_time=e.req_time,
                    method=e.method, result=e.result, url=e.url)
        datas.append(data)
    return jsonify(datas)


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
