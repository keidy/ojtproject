import os
from dotenv import load_dotenv

load_dotenv(verbose=True)
user = os.getenv("USER")
pw = os.getenv("PW")
host = os.getenv('RDS_ENDPOINT')
aws_db = {
    "user": user,
    "password": pw,
    "host": host,
    "port": "3306",  # Maria DB의 포트
    "database": "ojt",
}

SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{aws_db['user']}:{aws_db['password']}@{aws_db['host']}:{aws_db['port']}/{aws_db['database']}?charset=utf8"

