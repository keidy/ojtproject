# coding: utf-8
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Log(db.Model):
    __tablename__ = 'log'

    no = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(16), nullable=False)
    name = db.Column(db.String(30), nullable=True)
    req_time = db.Column(db.DateTime)
    method = db.Column(db.String(8))
    result = db.Column(db.String(4))
    url = db.Column(db.String(30))
    
    def __init__(self, no, ip, name, req_time, method, result, url) -> None:
        self.no = no
        self.ip = ip
        self.name = name
        self.req_time = req_time
        self.method = method
        self.result = result
        self.url = url
